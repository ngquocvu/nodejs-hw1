const express = require("express");
const fileRoute = express.Router();
const fs = require("fs");
const uploadFolder = "./upload";

fileRoute.get("/:fileName", async (req, res) => {
  fs.readFile(
    uploadFolder + `/${req.params.fileName}`,
    "utf-8",
    (err, data) => {
      if (err) {
        if (!data) {
          res.status(400).json({
            message: `No file with: '${req.params.fileName}' filename found`,
          });
        } else {
          res.status(500).json(err);
        }
      } else
        res.json({
          message: "Success",
          fileName: req.params.fileName,
          content: data,
          extension: req.params.fileName.split(".").pop(),
        });
    }
  );
});

fileRoute.get("/", (req, res) => {
  const list = [];
  fs.readdir(uploadFolder, (error, files) => {
    if (error) {
      res.status(500).send("Internal server error");
    }
    files.forEach((file) => {
      list.push(file);
    });
    res.status(200).json(list);
  });
});

fileRoute.post("/", (req, res) => {
  fs.appendFile(
    `${uploadFolder}/${req.body.filename}`,
    req.body.content,
    (err) => {
      if (err) {
        if (req.body.filename || req.body.content) {
          res
            .status(400)
            .json({ message: "Please specify 'content' parameter" });
        } else {
          res.status(500).json(err);
        }
      } else res.status(200).send({ message: "File created successfully" });
    }
  );
});

module.exports = fileRoute;
