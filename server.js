const express = require("express");
const bodyParser = require("body-parser");
const fileRoute = require("./fileRoute");
const morgan = require("morgan");
const app = express();

app.use(morgan("tiny"));
app.use(bodyParser.json());
app.use("/api/files", fileRoute);

app.listen(8080, () => {
  console.log("listening on 8080");
});
